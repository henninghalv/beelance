from django.test import TestCase
from projects.views import project_view, get_user_task_permissions
from projects.models import Task, Project, ProjectCategory, Team, TaskOffer
from user.models import Profile
from django.contrib.auth.models import User
from django.test.client import RequestFactory

class ProjectsViewsTests(TestCase):
    def setUp(self):
        user1 = User.objects.create_user(
            "ine123", "ine123@example.com", "inespassword"
        )
        profile1 = Profile.objects.filter(pk=user1.profile.pk).update(
            company = "Comp",
            phone_number = 9090,
            street_address = "blab",
            city =  "city",
            state = "state",
            postal_code = "postalcode",
            country = "country"
        )

        user2 = User.objects.create_user("ine2", "ine2@example.com", "ine2spassword")      
        user3 = User.objects.create_user("ine3", "ine3@example.com", "ine3spassword")
        user4 = User.objects.create_user("ine4", "ine4@example.com", "ine4spassword")
        user5 = User.objects.create_user("ine5", "ine5@example.com", "ine5spassword")
        user6 = User.objects.create_user("ine6", "ine6@example.com", "ine6spassword")
        user7 = User.objects.create_user("ine7", "ine7@example.com", "ine7spassword")
        user8 = User.objects.create_user("ine8", "ine8@example.com", "ine8spassword")

        category = ProjectCategory.objects.create(name="Gardening")

        project = Project.objects.create(
            user = Profile.objects.get(pk=profile1),
            title = "My Project",
            description = "Descrip",
            category = ProjectCategory.objects.get(name="Gardening"),
            status = 'o'
        )
        
        task = Task.objects.create(
            title = "Tasktitle",
            description = "Task desc",
            budget = 100,
            project = project,
        )
        task.read.add(user3.profile)
        task.write.add(user5.profile)
        task.modify.add(user6.profile)

        task_offer = TaskOffer.objects.create(
            task = task,
            offerer = user8.profile,
            title = "OfferTitle",
            price = 12,
            description = "Desc",
            status = 'a'
        )

        team = Team.objects.create(
            name = 'A team',
            task = task,
        )

        team2 = Team.objects.create(
            name = 'B team',
            task = task,
            write = True
        )

        team.members.add(user4.profile)
        team2.members.add(user7.profile)

        #Request factory
        self.rf = RequestFactory()
        self.user1 = user1
        self.user2 = user2
        self.user3 = user3
        self.user4 = user4
        self.user5 = user5
        self.user6 = user6
        self.user7 = user7
        self.user8 = user8
        self.project = project
        self.task = task
        self.task_offer = task_offer


    """
    // get_user_task_permissions tests
    """

    def test_get_user_task_permissions_owner(self):       
        # user1 is owner and should have all permissions
        self.assertEqual(get_user_task_permissions(self.user1, self.task), {
            'write': True,
            'read': True,
            'modify': True,
            'owner': True,
            'upload': True,
        })
    def test_get_user_task_permissions_none(self):
        #user2 is not associated with the task and should have no permissions
        self.assertEqual(get_user_task_permissions(self.user2, self.task), {
            'write': False,
            'read': False,
            'modify': False,
            'owner': False,
            'view_task': False,
            'upload': False,
        })
    
    def test_get_user_task_permissions_accepted(self):
        #user8s offer has been accepted, and should have all permissions but owner
        self.assertEqual(get_user_task_permissions(self.user8, self.task), {
            'write': True,
            'read': True,
            'modify': True,
            'owner': False,
            'upload': True,
        })
        #No more test cases needed for full STATEMENT coverage

    def test_get_user_task_permissions_read(self):
        #user3 should have read permission
        self.assertEqual(get_user_task_permissions(self.user3, self.task), {
            'write': False,
            'read': True,
            'modify': False,
            'owner': False,
            'view_task': False,
            'upload': False,
        })
    
    def test_get_user_task_permissions_team_no_write(self):
        #user4 is part of a no-write team associated with the task and should have view_task permission
        self.assertEqual(get_user_task_permissions(self.user4, self.task), {
            'write': False,
            'read': False,
            'modify': False,
            'owner': False,
            'view_task': True,
            'upload': False,
        })

    def test_get_user_task_permissions_write(self):
        #user5 have write access
        self.assertEqual(get_user_task_permissions(self.user5, self.task), {
            'write': True,
            'read': False,
            'modify': False,
            'owner': False,
            'view_task': False,
            'upload': False,
        })

    def test_get_user_task_permissions_modify(self):
        #user6 have modify access
        self.assertEqual(get_user_task_permissions(self.user6, self.task), {
            'write': False,
            'read': False,
            'modify': True,
            'owner': False,
            'view_task': False,
            'upload': False,
        })

    def test_get_user_task_permissions_team_write(self):
        #user7 is part of a write team associated with the task and should have upload and view_task permission
        self.assertEqual(get_user_task_permissions(self.user7, self.task), {
            'write': False,
            'read': False,
            'modify': False,
            'owner': False,
            'view_task': True,
            'upload': True,
        })


    """
        project_view tests
    """

    def test_project_view_get(self):
        get_request = self.rf.get('/projects/')
        get_request.user = self.user1
        response = project_view(get_request, self.project.id)
        self.assertEqual(response.status_code, 200)

    def test_project_view_post_offer_response(self):
        post_request = self.rf.post('/projects/', {
            "feedback":"yus",
            "status":"a",
            "taskofferid":self.task_offer.id,
            "offer_response":" "

        })
        post_request.user = self.user1
        response = project_view(post_request, self.project.id)
        self.assertEqual(response.status_code, 200)

    def test_project_view_post_status_change(self):
        post_request = self.rf.post('/projects/', {
            "status":'i',
            "status_change":""
        })
        post_request.user = self.user1
        response = project_view(post_request, self.project.id)
        self.assertEqual(response.status_code, 200) 

    def test_project_view_post_offer_submit(self):
        post_request = self.rf.post('/projects/', {
            "title":"Task offer",
            "description": "Desc",
            "price":"100",
            "taskvalue": self.task.id,
            "offer_submit":""
        })
        post_request.user = self.user2
        response = project_view(post_request, self.project.id)
        self.assertEqual(response.status_code, 200)


    