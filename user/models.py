from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    company = models.TextField(max_length=50, blank=True)
    phone_number = models.TextField(max_length=50, blank=True)
    street_address = models.TextField(max_length=50, blank=True)
    city = models.TextField(max_length=50, blank=True)
    state = models.TextField(max_length=50, blank=True)
    postal_code = models.TextField(max_length=50, blank=True)
    country = models.TextField(max_length=50, blank=True)
    categories = models.ManyToManyField('projects.ProjectCategory', related_name='competance_categories')
    rating = models.IntegerField(default=0)
    num_ratings = models.IntegerField(default=0)

    def __str__(self):
        return self.user.username

    def add_rating(self, rating):
        self.num_ratings = self.num_ratings + 1
        self.rating = self.rating + rating
        self.save()
    
    def get_rating(self):
        if self.num_ratings == 0:
            return "No ratings yet"
        else: 
            rating = self.rating / float(self.num_ratings)
            return "{0:.2f}".format(rating) 


@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()
